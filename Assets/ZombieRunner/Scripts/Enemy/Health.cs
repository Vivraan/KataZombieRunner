﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

namespace Kata.ZombieRunner.Enemy
{
    public class Health : Core.Health
    {
        [Header("Animator Parameters"), SerializeField] string dieTrigger = "Die";
        [Space]
        [SerializeField] Collider weakSpot = null;
        [SerializeField, Min(1f)] float weakSpotDamageBonus = 2f;
        [Space]
        [SerializeField] UnityEvent onDamaged = null;

        Animator animator;
        Controller controller;
        NavMeshAgent navMeshAgent;

        void Awake()
        {
            navMeshAgent = GetComponent<NavMeshAgent>();
            controller = GetComponent<Controller>();
            animator = GetComponent<Animator>();
        }

        public void TakeDamage(in float damage, in Collider hitCollider)
        {
            TakeDamageImpl(hitCollider == weakSpot ? damage * weakSpotDamageBonus : damage);
            onDamaged.Invoke();
        }

        protected override void OnDeath()
        {
            animator.SetTrigger(dieTrigger);
            controller.enabled = false;
            navMeshAgent.enabled = false;
        }
    }
}
