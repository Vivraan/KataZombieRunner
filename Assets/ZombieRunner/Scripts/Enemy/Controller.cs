﻿using UnityEngine;
using UnityEngine.AI;

namespace Kata.ZombieRunner.Enemy
{
    public class Controller : MonoBehaviour
    {
        enum State { Idle, Provoked };

        [SerializeField, Min(0f)] float damage = 20f;
        [SerializeField, Min(0f)] float chaseRange = 5f;
        [SerializeField, Min(0f)] float turnSpeed = 1f;

        [Header("Animator Parameters")]
        [SerializeField] bool blendedMovement = false;
        [SerializeField] string idleTrigger = "Idle";
        [SerializeField] string moveTrigger = "Move";
        [SerializeField] string moveSpeedFloat = "MoveSpeed";
        [SerializeField] string attackBool = "Attack";
        Player.Health player = null;
        Animator animator;
        NavMeshAgent navMeshAgent;
        State state = State.Idle;

        void Awake()
        {
            player = FindObjectOfType<Player.Health>();
            animator = GetComponent<Animator>();
            navMeshAgent = GetComponent<NavMeshAgent>();
        }

        void Update()
        {
            if (blendedMovement)
            {
                animator.SetFloat(moveSpeedFloat, navMeshAgent.velocity.magnitude);
            }

            var distance = Vector3.Distance(transform.position, player.transform.position);

            if (!player.IsDead)
            {
                if (distance <= chaseRange)
                {
                    Provoke();
                }
            }
            else
            {
                Forget();
            }

            if (state == State.Provoked)
            {
                Engage();
            }

            void Forget()
            {
                state = State.Idle;
                animator.SetBool(attackBool, false);
                if (!blendedMovement)
                {
                    animator.SetTrigger(idleTrigger);
                }
            }

            void Engage()
            {
                navMeshAgent.SetDestination(player.transform.position);
                FacePlayer();

                animator.SetBool(attackBool, distance <= navMeshAgent.stoppingDistance);

                void FacePlayer()
                {
                    var facingDirection = player.transform.position - transform.position;
                    facingDirection.y = 0f;
                    facingDirection.Normalize();

                    var lookRotation = Quaternion.LookRotation(facingDirection);
                    transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * turnSpeed);
                }
            }
        }

        public void Provoke()
        {
            state = State.Provoked;
            if (!blendedMovement)
            {
                animator.SetTrigger(moveTrigger);
            }
        }

        void OnHitEvent()
        {
            player.TakeDamage(damage);
        }

        void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, chaseRange);
            Gizmos.color = Color.red;
            if (Application.isPlaying)
            {
                Gizmos.DrawWireSphere(transform.position, navMeshAgent.stoppingDistance);
            }
        }
    }
}
