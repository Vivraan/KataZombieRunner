﻿using UnityEngine;

namespace Kata.ZombieRunner.Core
{
    public abstract class Health : MonoBehaviour
    {
        [SerializeField, Min(1)] float maxHealth = 100;
        float currentHealth;

        void Start()
        {
            currentHealth = maxHealth;
        }

        protected void TakeDamageImpl(in float damage)
        {
            currentHealth = Mathf.Clamp(currentHealth - damage, 0f, maxHealth);

            if (currentHealth <= 0f)
            {
                OnDeath();
            }
        }

        protected abstract void OnDeath();
    }
}
