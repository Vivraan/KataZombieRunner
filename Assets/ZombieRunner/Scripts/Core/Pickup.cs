﻿using UnityEngine;

namespace Kata.ZombieRunner.Core
{
    public abstract class Pickup : MonoBehaviour
    {
        static readonly int EmissionColorId = Shader.PropertyToID("_EmissionColor");

        [Header("Flashing (disable script to turn this off)"), SerializeField, ColorUsage(showAlpha: true, hdr: true)]
        Color emissionShade = new Color(.5f, .5f, .5f);

        [SerializeField, Min(0f)] float frequency = 2f;
        Material[] materials;
        float randomSeed;
#if UNITY_EDITOR
        Color[] originalColors;
#endif

        void Awake()
        {
            if (!enabled) return;
            var childRenderer = GetComponentInChildren<Renderer>();
            if (!childRenderer) return;
            materials = childRenderer.sharedMaterials;
#if UNITY_EDITOR
            originalColors = new Color[materials.Length];
#endif
        }

        void OnEnable()
        {
            randomSeed = Random.value;
#if UNITY_EDITOR
            for (var i = 0; i < materials.Length; i++)
            {
                originalColors[i] = materials[i].GetColor(EmissionColorId);
            }
#endif
        }

#if UNITY_EDITOR
        void OnDisable()
        {
            for (var i = 0; i < materials.Length; i++)
            {
                materials[i].SetColor(EmissionColorId, originalColors[i]);
            }
        }
#endif

        void Update()
        {
            var glow = Mathf.Abs(Mathf.Sin((Time.time + randomSeed) * frequency));
            foreach (var material in materials)
            {
                material.SetColor(EmissionColorId, emissionShade * glow);
            }
        }

        void OnTriggerEnter(Collider other)
        {
            if (ReplenishQuantity(other))
            {
                Destroy(gameObject);
            }
        }

        protected abstract bool ReplenishQuantity(in Collider other);
    }
}