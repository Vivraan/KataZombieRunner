﻿using Kata.ZombieRunner.Core;
using UnityEngine;

namespace Kata.ZombieRunner.Weapons
{
    public class Suppressor : MonoBehaviour
    {
        const string SuppressorButton = "Fire2";
        [SerializeField, Min(0f)] Vector3 suppressorMuzzleFlashDisplacement = default;
        [SerializeField, Range(0f, 1f)] float suppressedMuzzleFlashFraction = .5f;
        [SerializeField, Range(0f, 1f)] float rangeHandicapFraction = .8f;
        [SerializeField, Range(0f, 1f)] float damageHandicapFraction = .7f;
        ParticleSystem.Burst originalMuzzleFlashBurst;
        ParticleSystem.Burst suppressedFlashBurst;
        Vector3 originalMuzzleFlashPosition;
        Vector3 suppressedFlashPosition;
        ParticleSystem muzzleFlash;
        MeshRenderer meshRenderer;

        bool IsSuppressed
        {
            get => meshRenderer.enabled;
            set => meshRenderer.enabled = value;
        }

        public float RangeHandicapFraction => rangeHandicapFraction;
        public float DamageHandicapFraction => damageHandicapFraction;

        void Awake()
        {
            muzzleFlash = GetComponentInParent<Weapon>().MuzzleFlash;
            meshRenderer = GetComponent<MeshRenderer>();
        }

        void Start()
        {
            originalMuzzleFlashBurst = muzzleFlash.emission.GetBurst(0);
            suppressedFlashBurst = new ParticleSystem.Burst(
                originalMuzzleFlashBurst.time,
                originalMuzzleFlashBurst.count.constant * suppressedMuzzleFlashFraction
            );

            originalMuzzleFlashPosition = muzzleFlash.transform.localPosition;
            suppressedFlashPosition = originalMuzzleFlashPosition + suppressorMuzzleFlashDisplacement;
        }

        void Update()
        {
            if (Input.GetButtonDown(SuppressorButton))
            {
                muzzleFlash.transform.localPosition = !IsSuppressed ? suppressedFlashPosition : originalMuzzleFlashPosition;
                IsSuppressed = !IsSuppressed;
            }
            
        }

        public void SuppressMuzzleFlash()
        {
            muzzleFlash.emission.SetBurst(0, IsSuppressed ? suppressedFlashBurst : originalMuzzleFlashBurst);
        }
    }
}
