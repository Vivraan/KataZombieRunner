﻿using Kata.ZombieRunner.Core;
using Kata.ZombieRunner.Player;
using TMPro;
using UnityEngine;

namespace Kata.ZombieRunner.Weapons
{
    [RequireComponent(typeof(Animator))]
    public class WeaponSwitcher : MonoBehaviour, IDeathListener
    {
        const string CycleWeaponsButton = "CycleWeapons";
        [SerializeField, Min(0)] int currentWeaponIndex = 0;
        // TODO should this be refactored?
        [SerializeField] TextMeshProUGUI ammoText = null;
        [SerializeField] TextMeshProUGUI weaponText = null;
        Weapon[] weapons;
        Ammo ammo;

        void Awake()
        {
            weapons = GetComponentsInChildren<Weapon>(includeInactive: true);
            ammo = GetComponentInParent<Ammo>();
        }

        void OnDisable()
        {
            // Takes care of disabling weapons, zooming, and suppressors
            foreach (var weapon in weapons)
            {
                weapon.gameObject.SetActive(false);
            }
        }

        void Start()
        {
            currentWeaponIndex = Mathf.Clamp(currentWeaponIndex, 0, weapons.Length - 1);
            
            for (var i = 0; i < weapons.Length; i++)
            {
                weapons[i].gameObject.SetActive(i == currentWeaponIndex);
            }
        }

        void Update()
        {
            var previousWeaponIndex = currentWeaponIndex;

            ProcessNumberKeys();
            ProcessCycleWeaponsButton();

            if (previousWeaponIndex != currentWeaponIndex)
            {
                weapons[previousWeaponIndex].gameObject.SetActive(false);
                weapons[currentWeaponIndex].gameObject.SetActive(true);
            }

            ammoText.text = ammo.Slots[weapons[currentWeaponIndex].Caliber].ToString();
            weaponText.text = weapons[currentWeaponIndex].name;

            void ProcessNumberKeys()
            {
                for (var i = 0; i < weapons.Length; i++)
                {
                    if (Input.GetButtonDown($"Weapon{i + 1}"))
                    {
                        currentWeaponIndex = i;
                    }
                }
            }

            void ProcessCycleWeaponsButton()
            {
                if (Input.GetButtonDown(CycleWeaponsButton))
                {
                    currentWeaponIndex = (currentWeaponIndex + 1) % weapons.Length;
                }
            }
        }
    }
}
