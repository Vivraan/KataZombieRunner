﻿using UnityEngine;

namespace Kata.ZombieRunner.Weapons
{
    public class Magazine : MonoBehaviour
    {
        MeshRenderer meshRenderer;

        void Awake()
        {
            meshRenderer = GetComponent<MeshRenderer>();
        }

        public void Show(in bool active)
        {
            meshRenderer.enabled = active;
        }
    }
}
