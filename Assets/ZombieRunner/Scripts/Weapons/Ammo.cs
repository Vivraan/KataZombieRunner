﻿using System;
using System.Collections.Generic;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

namespace Kata.ZombieRunner.Weapons
{
    public class Ammo : MonoBehaviour
    {
        [Serializable]
        public enum Caliber
        {
            _9mm,
            _12ga,
            _5_56x45mmNATO,
            __50calBMG,
        }

        [Serializable]
        class SlotsDictionary : SerializableDictionaryBase<Caliber, int> { }

        [SerializeField] SlotsDictionary slots = null;

        public IReadOnlyDictionary<Caliber, int> Slots => slots;

        public void AddToSlot(in Caliber caliber, in int amount)
        {
            slots[caliber] += Mathf.Max(0, amount);
        }

        public void Minus1FromSlot(in Caliber caliber)
        {
            slots[caliber] = Mathf.Max(0, slots[caliber] - 1);
        }
    }
}
