﻿using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Kata.ZombieRunner.Weapons
{
    public class TargetingReticule : MonoBehaviour
    {
        [SerializeField] Image reticule = null;
        [SerializeField] Image sniperView = null;

        public void EnableReticule(in bool active)
        {
            reticule.gameObject.SetActive(active);
        }

        public void EnableSniperView(in bool active)
        {
            sniperView.gameObject.SetActive(active);
        }
    }
}
