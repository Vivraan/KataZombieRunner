﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

namespace Kata.ZombieRunner.Weapons
{
    public class Weapon : MonoBehaviour
    {
        const string FireButton = "Fire1";

        [Header("Particle FX")] [SerializeField]
        ParticleSystem muzzleFlash = null;

        [SerializeField] ParticleSystem hitSparks = null;
        [Header("Misc")] [SerializeField] bool automatic = true;
        [SerializeField] Ammo.Caliber caliber = default;
        [SerializeField, Min(0f)] float range = 100f;
        [SerializeField, Min(1f)] float damage = 10f;
        [SerializeField, Range(0f, 1f)] float movementPenalty = 1f;
        [SerializeField, Min(0f)] float shotInterval = .5f;
        new Camera camera;
        Ammo ammo;
        Suppressor suppressor;
        Magazine magazine;
        bool canShoot = true;
        float timeBetweenShots;
        RigidbodyFirstPersonController fpController;

        static Vector3? originalSpeeds = null;
        static readonly Dictionary<Transform, Enemy.Health> Encounters = new Dictionary<Transform, Enemy.Health>();

        public Ammo.Caliber Caliber => caliber;
        public ParticleSystem MuzzleFlash => muzzleFlash;

        Vector3 MovementSpeeds
        {
            get => new Vector3(
                fpController.movementSettings.ForwardSpeed,
                fpController.movementSettings.BackwardSpeed,
                fpController.movementSettings.StrafeSpeed);

            set
            {
                fpController.movementSettings.ForwardSpeed = value.x;
                fpController.movementSettings.BackwardSpeed = value.y;
                fpController.movementSettings.StrafeSpeed = value.z;
            }
        }

        void Awake()
        {
            camera = Camera.main;
            ammo = GetComponentInParent<Ammo>();
            suppressor = GetComponentInChildren<Suppressor>();
            magazine = GetComponentInChildren<Magazine>();
            fpController = GetComponentInParent<RigidbodyFirstPersonController>();

            if (!originalSpeeds.HasValue)
            {
                originalSpeeds = MovementSpeeds;
            }
        }

        void OnEnable()
        {
            if (originalSpeeds.HasValue)
            {
                MovementSpeeds = originalSpeeds.Value * (1f - movementPenalty);
            }

            if (!canShoot)
            {
                var cooldownTimeRemaining = Time.time - timeBetweenShots;
                StartCoroutine(Cooldown(cooldownTimeRemaining));
            }

            IEnumerator Cooldown(float timeLeft)
            {
                yield return new WaitForSeconds(shotInterval - timeLeft);
                canShoot = true;
            }
        }

        void OnDisable()
        {
            if (originalSpeeds.HasValue)
            {
                MovementSpeeds = originalSpeeds.Value;
            }
        }

        void Update()
        {
            var hasBullets = ammo.Slots[caliber] > 0;
            if (magazine)
            {
                magazine.Show(hasBullets);
            }

            var fireButtonPoll = automatic ? Input.GetButton(FireButton) : Input.GetButtonDown(FireButton);
            if (!fireButtonPoll || !canShoot || !hasBullets) return;
// ReSharper disable Unity.PerformanceCriticalCodeInvocation
            StartCoroutine(Shoot());

            IEnumerator Shoot()
            {
                canShoot = false;
                timeBetweenShots = Time.time;
                {
                    var adjustedRange = range;
                    var adjustedDamage = damage;

                    PerformSuppressorLogic();
                    muzzleFlash.Play();
                    ammo.Minus1FromSlot(caliber);
                    HitScan();
                    yield return new WaitForSeconds(shotInterval);


                    void PerformSuppressorLogic()
                    {
                        if (!suppressor) return;
                        suppressor.SuppressMuzzleFlash();
                        adjustedRange *= suppressor.RangeHandicapFraction;
                        adjustedDamage *= suppressor.DamageHandicapFraction;
                    }

                    void HitScan()
                    {
                        var cam = camera.transform;
                        if (!Physics.Raycast(cam.position, cam.forward, out var hit, adjustedRange)) return;

                        SpawnSparks();
                        DamageCachedEnemies();

                        void SpawnSparks()
                        {
                            var sparks = Instantiate(hitSparks, hit.point, Quaternion.LookRotation(hit.normal));
                            Destroy(sparks.gameObject, sparks.main.duration);
                        }

                        void DamageCachedEnemies()
                        {
                            if (Encounters.TryGetValue(hit.transform, out var enemyHealth))
                            {
                                if (enemyHealth)
                                {
                                    print($"Hit {hit.collider}");
                                    enemyHealth.TakeDamage(adjustedDamage, hit.collider);
                                }
                            }
                            else
                            {
// ReSharper restore Unity.PerformanceCriticalCodeInvocation
                                enemyHealth = hit.transform.GetComponentInParent<Enemy.Health>();
                                // add it regardless of whether it is null or not - remember everything hit!
                                Encounters.Add(hit.transform, enemyHealth);
                            }
                        }
                    }
                }
                canShoot = true;
                timeBetweenShots = 0f;
            }
        }
    }
}