﻿using Kata.ZombieRunner.Core;
using UnityEngine;
using UnityEngine.Serialization;
using UnityStandardAssets.Characters.FirstPerson;

namespace Kata.ZombieRunner.Weapons
{
    public class Zoom : MonoBehaviour
    {
        const float MinFieldOfView = 1f;
        const float MaxFieldOfView = 179f;
        const string ZoomButton = "Zoom";
        [SerializeField] new Cinemachine.CinemachineVirtualCamera camera = null;

        [Header("Aim Down Sights Animation")]
        [SerializeField] string zoomBool = "Zoom";
        [Space]
        [SerializeField] bool showReticule = true;
        [SerializeField, Min(1f)] float[] zoomMultipliers = null;
        int numZoomLevels;
        float[] fieldsOfView;
        Vector2[] sensitivities;
        int currentZoomIdx;
        MeshRenderer[] meshesToHide;
        RigidbodyFirstPersonController fpController;
        Animator aimAnimator;
        TargetingReticule reticule;
        Scope scope;

        bool IsZoomed => currentZoomIdx != 0;

        void Awake()
        {
            scope = GetComponentInChildren<Scope>();
            meshesToHide = scope ? GetComponentsInChildren<MeshRenderer>() : new MeshRenderer[0];
            fpController = GetComponentInParent<RigidbodyFirstPersonController>();
            aimAnimator = GetComponentInParent<WeaponSwitcher>().GetComponent<Animator>();
            reticule = FindObjectOfType<TargetingReticule>();
        }

        void OnEnable()
        {
            reticule.EnableReticule(showReticule && !IsZoomed);
            reticule.EnableSniperView(false);

            if (fieldsOfView == null || sensitivities == null)
            {
                numZoomLevels = zoomMultipliers.Length + 1;
                fieldsOfView = new float[numZoomLevels];
                sensitivities = new Vector2[numZoomLevels];
                InitFromZoom();
            }

            void InitFromZoom()
            {
                fieldsOfView[0] = camera.m_Lens.FieldOfView;
                sensitivities[0] = new Vector2(fpController.mouseLook.XSensitivity, fpController.mouseLook.YSensitivity);

                for (var i = 1; i < numZoomLevels; i++)
                {
                    fieldsOfView[i] = Mathf.Clamp(fieldsOfView[0] / zoomMultipliers[i - 1], MinFieldOfView, MaxFieldOfView);
                    sensitivities[i] = sensitivities[0] / zoomMultipliers[i - 1];
                }
            }
        }

        void OnDisable()
        {
            currentZoomIdx = 0;
            SetZoom();
        }


        // Update is called once per frame
        void Update()
        {
            if (Input.GetButtonDown(ZoomButton))
            {
                currentZoomIdx = (currentZoomIdx + 1) % numZoomLevels;
                SetZoom();
                SetupTargetingReticule();
            }

            void SetupTargetingReticule()
            {
                if (scope)
                {
                    reticule.EnableSniperView(IsZoomed);
                    reticule.EnableReticule(IsZoomed);
                }
                else
                {
                    reticule.EnableReticule(showReticule && !IsZoomed);
                }
            }
        }

        void SetZoom()
        {
            aimAnimator.SetBool(zoomBool, IsZoomed);
            camera.m_Lens.FieldOfView = fieldsOfView[currentZoomIdx];
            fpController.mouseLook.XSensitivity = sensitivities[currentZoomIdx].x;
            fpController.mouseLook.YSensitivity = sensitivities[currentZoomIdx].y;
            
            foreach (var meshRenderer in meshesToHide)
            {
                meshRenderer.enabled = !IsZoomed;
            }
        }
    }
}
