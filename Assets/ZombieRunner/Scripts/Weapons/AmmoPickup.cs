﻿using Kata.ZombieRunner.Core;
using UnityEngine;

namespace Kata.ZombieRunner.Weapons
{
    public class AmmoPickup : Pickup
    {
        [Header("Ammo Pickup")]
        [SerializeField] Ammo.Caliber caliber = default;
        [SerializeField, Min(1)] int amount = 10;

        protected override bool ReplenishQuantity(in Collider other)
        {
            if (!other.TryGetComponent(out Ammo ammo)) return false;
            ammo.AddToSlot(caliber, amount);
            return true;
        }
    }
}
