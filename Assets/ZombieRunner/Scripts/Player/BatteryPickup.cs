﻿using Kata.ZombieRunner.Core;
using UnityEngine;

namespace Kata.ZombieRunner.Player
{
    public class BatteryPickup : Pickup
    {
        [Header("Battery Pickup")]
        [SerializeField, Range(0f, Flashlight.MaxAngle - Flashlight.MinAngle)]
        float angleIncrease = 10f;

        [SerializeField, Range(0f, Flashlight.MaxIntensity - Flashlight.MinIntensity)]
        float intensityIncrease = 1f;
        
        protected override bool ReplenishQuantity(in Collider other)
        {
            var flashlight = other.GetComponentInChildren<Flashlight>();
            if (!flashlight) return false;
            flashlight.Recharge(angleIncrease, intensityIncrease);
            return true;
        }
    }
}