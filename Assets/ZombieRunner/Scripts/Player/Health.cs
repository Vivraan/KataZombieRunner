﻿using Kata.ZombieRunner.Core;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityStandardAssets.Characters.FirstPerson;

namespace Kata.ZombieRunner.Player
{
    public class Health : Core.Health
    {
        [Space] [SerializeField] UnityEvent onDamagedUi = null;

        [FormerlySerializedAs("onDeathUI"), SerializeField]
        UnityEvent onDeathUi = null;

        IDeathListener[] deathListeners;
        RigidbodyFirstPersonController fpController;
        new Rigidbody rigidbody;

        public bool IsDead => !enabled;

        void Awake()
        {
            rigidbody = GetComponent<Rigidbody>();
            fpController = GetComponent<RigidbodyFirstPersonController>();
            deathListeners = GetComponentsInChildren<IDeathListener>();
        }

        public void TakeDamage(in float damage)
        {
            onDamagedUi.Invoke();
            TakeDamageImpl(damage);
        }

        protected override void OnDeath()
        {
            onDeathUi.Invoke();

            fpController.enabled = false;
            rigidbody.velocity = Vector3.zero;
            rigidbody.isKinematic = true;
            foreach (var deathListener in deathListeners)
            {
                deathListener.enabled = false;
            }

            enabled = false;
        }
    }
}