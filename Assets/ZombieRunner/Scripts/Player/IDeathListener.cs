﻿namespace Kata.ZombieRunner.Player
{
    /// <summary>
    /// Acts as a marker for MonoBehaviours which need to be disabled when the player dies.
    /// </summary>
    public interface IDeathListener
    {
        bool enabled { set; }
    }
}
