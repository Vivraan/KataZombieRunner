﻿using Kata.ZombieRunner.Core;
using UnityEngine;

namespace Kata.ZombieRunner.Player
{
    public class Flashlight : MonoBehaviour, IDeathListener
    {
        public const float MinAngle = 30f;
        public const float MaxAngle = 70f;
        public const float MinIntensity = 0f;
        public const float MaxIntensity = 4f;
        const string FlashlightButton = "Flashlight";
        [SerializeField, Min(0f)] float intensityDecayRate = .1f;
        [Tooltip("Degrees/s"), SerializeField, Min(0f)] float angleDecayRate = 1f;
        [Tooltip("Degrees."), SerializeField, Range(MinAngle, MaxAngle)] float minAngle = 40f;
        new Light light;

        void Awake()
        {
            light = GetComponent<Light>();
            light.enabled = false;
            light.spotAngle = Mathf.Clamp(light.spotAngle, minAngle, MaxAngle);
            light.intensity = Mathf.Min(light.intensity, MaxIntensity);
        }

        void Update()
        {
            if (Input.GetButtonDown(FlashlightButton))
            {
                light.enabled = !light.enabled;
            }

            if (!light.enabled) return;
            
            DecreaseAngle();
            DecreaseIntensity();

            void DecreaseAngle()
            {
                if (light.spotAngle <= minAngle) return;
                light.spotAngle -= angleDecayRate * Time.deltaTime;
            }

            void DecreaseIntensity()
            {
                if (light.intensity <= 0f) return;
                light.intensity -= intensityDecayRate * Time.deltaTime;
            }
        }

        public void Recharge(in float angleIncrease, in float intensityIncrease)
        {
            light.spotAngle = Mathf.Clamp(light.spotAngle + angleIncrease, MinAngle, MaxAngle);
            light.intensity = Mathf.Clamp(light.intensity + intensityIncrease, MinIntensity, MaxIntensity);
        }
    }
}
