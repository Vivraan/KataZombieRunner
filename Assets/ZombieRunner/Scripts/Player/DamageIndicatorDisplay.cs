﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Kata.ZombieRunner.Player
{
    public class DamageIndicatorDisplay : MonoBehaviour
    {
        [SerializeField, Range(0f, 1f)] float fadeDuration = 3f;
        [SerializeField, Min(0f)] float fadeDelay = 1f;
        CanvasGroup canvasGroup;
        Coroutine activeFadeEffect;

        void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();
        }

        public void Flash(bool fadeOut)
        {
            if (activeFadeEffect != null)
            {
                StopCoroutine(activeFadeEffect);
            }

            canvasGroup.alpha = 1f;
            
            if (fadeOut)
            {
                activeFadeEffect = StartCoroutine(WaitThenFadeOut());
            }

            IEnumerator WaitThenFadeOut()
            {
                yield return new WaitForSeconds(fadeDelay);
                while (!Mathf.Approximately(canvasGroup.alpha, 0f))
                {
                    var deltaAlpha = Time.deltaTime / fadeDuration;
                    canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, 0f, deltaAlpha);
                    yield return new WaitForEndOfFrame();
                }
            }   
        }
    }
}
